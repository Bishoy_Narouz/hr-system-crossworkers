const ErrorMessageEnum =
{
    UNAUTHORIZED: 'UNAUTHORIZED',
    UNAUTHENTICATED: 'UNAUTHENTICATED',
    INVALID_TOKEN: 'INVALID_TOKEN',
    TOKEN_EXPIRED_ERROR: 'TOKEN_EXPIRED_ERROR',
    REQUIRED: 'REQUIRED',
    BAD_REQUEST: 'BAD_REQUEST',
    USER_IS_DUPLICATED: 'Username or Email Already Exists',
    RESTAURANT_IS_DUPLICATED: 'Restaurant Already Exists',
    INVALID_ROLE: 'User Role is not valid',
    USER_IS_NOT_FOUND: 'User is not found',
    RESTAURANT_IS_NOT_FOUND: 'Restaurant is not found',
    USER_IS_BLOCKED: 'User is blocked',
    INVALID_CREDENTIAL: 'Invalid Credential',
    CURRENT_PASSWORD_INCORRECT: 'Current Password is not correct',
    INVALID_RESET_PASSWORD_CODE: 'Password Code is not valid',
    CATEGORY_IS_NOT_FOUND: 'Category is not found',
    CATEGORY_IS_DUPLICATED: 'Category Already Exists',
    PRODUCT_IS_NOT_FOUND: 'Product is not found',
    MANAGER_IS_NOT_FOUND: 'Manager is not found',
    COMPANY_BENEFIT_IS_NOT_FOUND: 'Company Benefit is not found',
    PRODUCT_IS_DUPLICATED: 'Product Already Exists',
    COMPANY_BENEFIT_IS_DUPLICATED: 'Company Benefit Already Exists',
    MANAGER_IS_DUPLICATED: 'Manager Already Exists',
    MANAGER_IS_NOT_FOUND: 'Manager is not found'
}

module.exports = ErrorMessageEnum