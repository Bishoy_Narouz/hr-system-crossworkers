const env = typeof process.env.NODE_ENV === 'undefined' ? 'local' : process.env.NODE_ENV
const config = require(`./${env}`)

module.exports = {
    BackendHost: config.BackendHost,
    BackendPort: config.BackendPort,
    BackendUrl: config.BackendUrl,
    SocketPort: config.SocketPort,
    DBUrl: config.DBUrl,
    SaltRounds: config.SaltRounds,
    corsOptions: {
        origin: '*',
        optionsSuccessStatus: 200
    },
    RequestSizeLimit: '20mb',
    swaggerOptions: config.swaggerOptions,
    Token: config.Token,
    RefreshToken: config.RefreshToken
}