module.exports = {
    BackendHost: 'localhost',
    BackendPort: 5000,
    SocketPort: 4000,
    BackendUrl: 'http://localhost:5000/',
    DBUrl: 'mongodb://localhost:27017/hr-system',
    SaltRounds: 10,
    swaggerOptions: {
        explorer: true
    },
    Token: {
        Key: '0AQ9E8vTH(~3^AsMttbXR4B^TQg0r)BEH8DgeqRCbUJLgkREUtlnv@wmQU9WZPiA+qJW4mHHr>MmyEUSCt5UC>=Ft:7CzoSYRkQ',
        LifeTime: '3h'
    },
    RefreshToken: {
        Key: 'qRCbUJLgkREUtlnv@w0AQ9E8vTHBEH8DgemQU(~3^AsMttbXR4B^TQg0r)9WZPiA+qJW4zoSYRkQmHHr>MmyEUSCt5UC>=Ft:7C',
        LifeTime: '10d'
    }
}