import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import config from './Config'
import dbConfig from './Database/DBConfig'
import swaggerUi from 'swagger-ui-express'
import YAML from 'yamljs'
import AuthRoutes from './Modules/Auth/index'
import UserRoutes from './Modules/User/index'
import ManagerRoutes from './Modules/Manager/index'
import BenefitRoutes from './Modules/CompanyBenefit/index'
import ChatRoutes from './Modules/Chat/index'

const app = express()
app.use(bodyParser.json({ limit: config.RequestSizeLimit }))
app.use(bodyParser.urlencoded({ limit: config.RequestSizeLimit, extended: true }))
app.use(cors(config.corsOptions))

const socketIO = require('./MiddleWare/Services/SocketIO')(app, config)

app.use('/api/auth', AuthRoutes)
app.use('/api/user', UserRoutes)
app.use('/api/manager', ManagerRoutes)
app.use('/api/benefit', BenefitRoutes)
app.use('/api/chat', ChatRoutes)

const swaggerDocument = YAML.load('./swagger.yaml')
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, config.swaggerOptions))

app.listen(config.BackendPort, () => console.log(`the app listening on port ${config.BackendPort}`))
