import socketIO from 'socket.io'
import http from 'http'

module.exports = function (app, config) {
    const server = http.Server(app);
    const io = socketIO(server, config.SocketOptions)
    app.io = io
    io.on('connection', function (socket) {
        console.log('user connected')
        socket.on('new-message', function (data) {
            io.sockets.emit('new-message', data)
        });
        socket.on('disconnect', function () { console.log('disconnect') })
    })

    server.listen(config.SocketPort, () => console.log(`socket working on port ${config.SocketPort}`))
}