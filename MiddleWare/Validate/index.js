import validateString from './Base/string'
import validateEmail from './Base/email'
import validateRole from './Base/role'
import validateObjectId from './Base/objectId'
import validatePassword from './Custom/validatePassword'
import spacesNotAllowed from './Custom/notAllowedSpaces'
import validateToken from './Custom/validateToken'

module.exports = {
    validateString: validateString,
    validateEmail: validateEmail,
    validateRole: validateRole,
    validateObjectId: validateObjectId,
    validatePassword: validatePassword,
    spacesNotAllowed: spacesNotAllowed,
    validateToken: validateToken
}