import validator from 'validator'
import errors from '../../Errors/index'

module.exports = function (password) {
    if (!password) {
        throw new errors.Missing({ fieldName: 'Password' })
    }
    if (validator.contains(password, ' ')) {
        throw new errors.Invalid({ message: `Spaces is not Allowed in Password` })
    }
    if (typeof password !== 'string') {
        throw new errors.Invalid({ message: `Password must be string` })
    }
    if (password.length < 8) {
        throw new errors.Invalid({ message: `Password minimum length must be greater than 8 digits` })
    }

    if (password.length > 25) {
        throw new errors.Invalid({ message: `Password maximum length must be less than 25 digits` })
    }
    const capitalLetterPattern = new RegExp('^[a-zA-Z0-9]+$')
    if (!!capitalLetterPattern.test(password)) {
        throw new errors.Invalid({ message: `Password must be contains letters and numbers` })
    }
    else {
        return password
    }
}
