import validator from 'validator'
import errors from '../../Errors/index'

module.exports = function ({ string, fieldName } = {}) {
    if (validator.contains(string, ' ')) {
        throw new errors.Invalid({ message: `Spaces is not Allowed in ${fieldName}` })
    }
    else {
        return string
    }
}
