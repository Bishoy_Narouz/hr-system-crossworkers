import jwt from 'jsonwebtoken'
import config from '../../../Config'
import errors from '../../Errors/index'

module.exports = function (accessRoles = []) {
    return function (req, res, next) {
        let token = req.headers['x-access-token'] || req.headers['authorization'] || req.headers['token']
        if (token && token.startsWith('Bearer ')) {
            // Remove Bearer from string
            token = token.slice(7, token.length)
        }
        if (token) {
            jwt.verify(token, config.Token.Key, (err, decoded) => {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        res.send(new errors.ExpiredTokenError())
                    } else if (err.name === 'JsonWebTokenError') {
                        res.send(new errors.InvalidTokenError())
                    } else {
                        res.send(new errors.BadRequestError())
                    }
                } else {
                    req.decoded = decoded;
                    const userRole = (req.decoded && req.decoded.role) ? req.decoded.role : null
                    if (accessRoles.length >= 1) {
                        if (!userRole || !accessRoles.includes(userRole)) {
                            res.send(new errors.UnAuthorizedError())
                        }
                        else {
                            next()
                        }
                    } else {
                        next()
                    }
                }
            })
        } else {
            res.send(new errors.UnAuthenticatedError())
        }
    }
}