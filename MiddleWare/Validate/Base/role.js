import errors from '../../Errors/index'
import rolesEnum from '../../../Enums/RolesEnum'
import errorMessageEnum from '../../../Enums/ErrorMessageEnum'

module.exports = function ({ role, optional } = {}) {
    if (!optional) {
        if (!role) {
            throw new errors.Missing({ fieldName: 'Role' })
        } else if (![rolesEnum.EMPLOYEE, rolesEnum.HR_ASSISTANT].includes(role) || typeof role !== 'string') {
            throw new errors.Invalid({ message: errorMessageEnum.INVALID_ROLE })
        }
    }
    else if (role) {
        if (role === undefined) {
            return;
        }
        else if (role === null) {
            return null;
        }
        else if (![rolesEnum.EMPLOYEE, rolesEnum.HR_ASSISTANT].includes(role) || typeof role !== 'string') {
            throw new errors.Invalid({ message: errorMessageEnum.INVALID_ROLE })
        }
    }
}
