import validator from 'validator'
import errors from '../../Errors/index'

module.exports = function ({ number, fieldName, optional, min, max } = {}) {
    if (!optional) {
        if (!number) {
            throw new errors.Missing({ fieldName })
        } else if ((typeof number !== 'number')) {
            throw new errors.Invalid({ message: `${fieldName} must be Number` })
        }
    } else if (optional) {
        if (!number) {
            return
        } else if (typeof number !== 'number') {
            throw new errors.Invalid({ message: `${fieldName} must be Number` })
        }
    }

    if (max && number > max)
        throw new errors.Invalid({ message: `${fieldName} number exceeds the maximum value` })

    if (min && number < min)
        throw new errors.Invalid({ message: `${fieldName} number exceeds the minimum value` })

    return number
}
