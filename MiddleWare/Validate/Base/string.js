import errors from '../../Errors/index'

/* Validates a string is valid according a set of criteria */
module.exports = function string({ string, fieldName, optional, min, max, allowedSpecialCharacters } = {}) {
    if (!optional) {
        if (!string) {
            throw new errors.Missing({ fieldName })
        } else if (typeof string !== 'string') {
            throw new errors.Invalid({ message: `${fieldName} must be String` })
        }
    }
    else if (optional) {
        if (string === undefined) {
            return;
        }
        else if (string === null) {
            return null;
        }
        else if (typeof string !== 'string') {
            throw new errors.Invalid({ message: `${fieldName} must be String` })
        }
    }

    if (max && string.length > max)
        throw new errors.Invalid({ message: `${fieldName} exceeds the maximum length` })

    if (min && string.length < min)
        throw new errors.Invalid({ message: `${fieldName} is less than the minimum length` })

    let stringToValidate = string;
    if (allowedSpecialCharacters) {
        if (Array.isArray(allowedSpecialCharacters)) {
            for (let allowedSpecialCharacter of allowedSpecialCharacters) {
                if (allowedSpecialCharacter === '.')
                    allowedSpecialCharacter = '\\' + allowedSpecialCharacter

                stringToValidate = stringToValidate.replace(new RegExp(allowedSpecialCharacter, 'g'), '')
            }
        }
        else if (typeof allowedSpecialCharacters === 'string') {
            stringToValidate = stringToValidate.replace(new RegExp(allowedSpecialCharacters, 'g'), '')
        }
        else {
            throw new errors.BackendCode({
                message: `${fieldName} allowedSpecialCharacters needs to be an array or a string`
            })
        }
    }

    return string
}
