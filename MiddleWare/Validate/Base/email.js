import errors from '../../Errors/index'
import validator from 'validator'

module.exports = function ({ email, optional } = {}) {
    if (!optional) {
        if (!email) {
            throw new errors.Missing({ fieldName: 'Email' })
        } else if (!validator.isEmail(email)) {
            throw new errors.Invalid({ message: `Invalid Email` })
        }
    }
    else if (email) {
        if (email === undefined) {
            return;
        }
        else if (email === null) {
            return null;
        }
        else if (!validator.isEmail(email)) {
            throw new errors.Invalid({ message: `Invalid Email` })
        }
    }
}
