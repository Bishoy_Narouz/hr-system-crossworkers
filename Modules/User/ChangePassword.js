import bcrypt from 'bcrypt'
import config from '../../Config'

module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.put('/changePassword', validateToken([rolesEnum.HR_ASSISTANT, rolesEnum.EMPLOYEE]), async (req, res) => {
        try {
            const userId = req.decoded.userId
            const currentPassword = req.body.currentPassword
            let user = new db.User({
                _id: userId,
                password: req.body.newPassword
            })

            validate.validateString({ string: currentPassword, fieldName: 'Current Password', optional: false })
            validate.validatePassword(req.body.newPassword)

            const userCurrentPassword = await db.User.findOne({ _id: userId }).select('password')

            if (!bcrypt.compareSync(currentPassword, userCurrentPassword.password)) {
                throw new errors.Invalid({ message: errorMessageEnum.CURRENT_PASSWORD_INCORRECT })
            }

            const salt = bcrypt.genSaltSync(config.SaltRounds)
            user.password = bcrypt.hashSync(user.password, salt)
            await db.User.updateOne({ _id: userId }, user)
            res.send(new Success())
        } catch (error) {
            res.send(error)
        }
    })
}