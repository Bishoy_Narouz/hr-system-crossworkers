module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.put('/updateMyProfile', validateToken([rolesEnum.MANAGER, rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            const user = new db.User({
                _id: req.decoded.userId,
                fullName: req.body.fullName,
                username: req.body.username,
                email: req.body.email,
                mobile: req.body.mobile
            })

            validate.validateObjectId({ objectId: req.decoded.userId, fieldName: 'User Id', optional: false })
            validate.validateString({ string: user.username, fieldName: 'Username', optional: false, min: 3, max: 20 })
            validate.validateString({ string: user.fullName, fieldName: 'Full Name', optional: false, min: 6, max: 30 })
            validate.validateString({ string: user.mobile, fieldName: 'Mobile', optional: false })
            validate.validateEmail({ email: user.email, optional: false })

            await db.User.updateOne({ _id: user._id }, user)

            res.send(new Success())
        } catch (error) {
            res.send(error)
        }
    })
}