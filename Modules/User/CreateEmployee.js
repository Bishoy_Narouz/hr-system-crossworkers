import bcrypt from 'bcrypt'
import config from '../../Config'

module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.post('/createEmployee', validateToken([rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            let user = new db.User({
                fullName: req.body.fullName,
                username: req.body.username,
                email: req.body.email,
                password: req.body.password,
                mobile: req.body.mobile,
                role: rolesEnum.EMPLOYEE,
                manager: req.body.manager,
                createdAt: new Date()
            })

            validate.validateString({ string: user.username, fieldName: 'Username', optional: false, min: 3, max: 20 })
            validate.validateString({ string: user.fullName, fieldName: 'Full Name', optional: false, min: 6, max: 30 })
            validate.validateString({ string: user.mobile, fieldName: 'Mobile', optional: false })
            validate.validateEmail({ email: user.email, optional: false })
            validate.validatePassword(user.password)

            const isManagerExist = await db.Manager.findOne({ _id: req.body.manager })

            if (!isManagerExist) {
                next(new errors.NotFound({ message: errorMessageEnum.MANAGER_IS_NOT_FOUND }))
            }

            const salt = bcrypt.genSaltSync(config.SaltRounds)
            user.password = bcrypt.hashSync(user.password, salt)

            let result = await user.save()

            res.send(new Success(result))
        } catch (error) {
            res.send(error)
        }
    })
}