module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.put('/editEmployee/:_id', validateToken([rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            const user = new db.User({
                _id: req.params._id,
                fullName: req.body.fullName,
                username: req.body.username,
                email: req.body.email,
                mobile: req.body.mobile,
                manager: req.body.manager
            })

            validate.validateObjectId({ objectId: req.params._id, fieldName: 'Employee Id', optional: false })
            validate.validateObjectId({ objectId: req.body.manager, fieldName: 'Manager Id', optional: false })
            validate.validateString({ string: user.username, fieldName: 'Username', optional: false, min: 3, max: 20 })
            validate.validateString({ string: user.fullName, fieldName: 'Full Name', optional: false, min: 6, max: 30 })
            validate.validateString({ string: user.mobile, fieldName: 'Mobile', optional: false })
            validate.validateEmail({ email: user.email, optional: false })

            const isManagerExist = await db.Manager.findOne({ _id: req.body.manager })

            if (!isManagerExist) {
                next(new errors.NotFound({ message: errorMessageEnum.MANAGER_IS_NOT_FOUND }))
            }

            await db.User.updateOne({ _id: user._id }, user)

            res.send(new Success())
        } catch (error) {
            res.send(error)
        }
    })
}