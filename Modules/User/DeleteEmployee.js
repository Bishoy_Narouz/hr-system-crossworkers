module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.delete('/deleteEmployee/:_id', validateToken([rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            const _id = req.params._id
            await db.User.deleteOne({ _id: _id })
            res.send(new Success())
        } catch (error) {
            res.send(error)
        }
    })
}