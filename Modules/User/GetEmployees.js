module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.get('/getEmployees', validateToken([rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            const result = await db.User.find({ role: rolesEnum.EMPLOYEE })
                .select('fullName username email mobile manager')
                .populate('manager', 'name')
            res.send(new Success(result))
        } catch (error) {
            res.send(error)
        }
    })
}