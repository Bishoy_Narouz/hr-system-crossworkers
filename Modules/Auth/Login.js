import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import config from '../../Config'

module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.post('/login', async (req, res) => {
        try {
            const credential = {
                identity: req.body.identity,
                password: req.body.password
            }

            validate.validateString({ string: credential.identity, fieldName: 'Identity', optional: false })
            validate.validateString({ string: credential.password, fieldName: 'Password', optional: false })

            const user = await db.User.findOne(
                {
                    $and: [
                        { role: { $in: [rolesEnum.EMPLOYEE, rolesEnum.HR_ASSISTANT] } },
                        {
                            $or: [
                                { username: credential.identity },
                                { email: credential.identity }
                            ]
                        }
                    ]
                }
            )

            if (!user || !bcrypt.compareSync(credential.password, user.password)) {
                throw new errors.Invalid({ message: errorMessageEnum.INVALID_CREDENTIAL })
            }
            if (user.blocked) {
                throw new errors.Invalid({ message: errorMessageEnum.USER_IS_BLOCKED })
            }

            const token = jwt.sign({ userId: user._id, role: user.role }, config.Token.Key, { expiresIn: config.Token.LifeTime })
            const refreshToken = jwt.sign({ userId: user._id, role: user.role }, config.RefreshToken.Key, { expiresIn: config.RefreshToken.LifeTime })

            let result = {
                currentUser: {
                    fullName: user.fullName,
                    username: user.username,
                    email: user.email,
                    mobile: user.mobile,
                    role: user.role
                },
                tokens: {
                    token: token,
                    refreshToken: refreshToken
                }
            }

            res.send(new Success(result))
        } catch (error) {
            console.log(error)
            res.send(error)
        }
    })
}