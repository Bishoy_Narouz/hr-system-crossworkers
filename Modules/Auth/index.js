import fs from 'fs'
import express from 'express'
import Success from '../../MiddleWare/Errors/Models/SuccessResponse'
import errors from '../../MiddleWare/Errors/index'
import db from '../../Database/Models/index'
import validate from '../../MiddleWare/Validate/index'
import errorMessageEnum from '../../Enums/ErrorMessageEnum'
import validateToken from '../../MiddleWare/Validate/Custom/validateToken'
import rolesEnum from '../../Enums/RolesEnum'

const router = express.Router()
const routesPath = __dirname
let routes = []

fs
    .readdirSync(routesPath)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        if (file !== 'index.js') {
            let api = require(`./${file}`)(router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors)
            routes.push(api)
        }
    })

module.exports = routes

