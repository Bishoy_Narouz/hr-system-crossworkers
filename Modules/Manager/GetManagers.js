module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.get('/getManagers', validateToken([rolesEnum.EMPLOYEE, rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            const result = await db.Manager.find()
            res.send(new Success(result))
        } catch (error) {
            res.send(error)
        }
    })
}