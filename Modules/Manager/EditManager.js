module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.put('/editManager/:_id', validateToken([rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            let manager = new db.Manager({
                _id: req.params._id,
                name: req.body.name,
                manager: req.body.manager
            })

            validate.validateObjectId({ objectId: req.params._id, fieldName: 'Manager Id', optional: false })
            validate.validateString({ string: manager.name, fieldName: 'Name', optional: false, min: 3, max: 20 })

            await db.Manager.updateOne({ _id: manager._id }, manager)
            res.send(new Success())
        } catch (error) {
            console.log(error)
            res.send(error)
        }
    })
}