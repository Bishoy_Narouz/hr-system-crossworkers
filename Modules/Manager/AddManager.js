module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.post('/addManager', validateToken([rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            let manager = new db.Manager({
                name: req.body.name,
                createdAt: new Date()
            })

            validate.validateString({ string: manager.name, fieldName: 'Name', optional: false, min: 3, max: 20 })

            const result = await manager.save()
            res.send(new Success(result))
        } catch (error) {
            res.send(error)
        }
    })
}