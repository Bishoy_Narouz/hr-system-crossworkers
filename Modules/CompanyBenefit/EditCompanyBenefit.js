module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.put('/editCompanyBenefit/:_id', validateToken([rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            let benefit = new db.CompanyBenefit({
                _id: req.params._id,
                title: req.body.title,
                description: req.body.description
            })

            validate.validateObjectId({ objectId: req.params._id, fieldName: 'Benefit Id', optional: false })
            validate.validateString({ string: benefit.title, fieldName: 'Title', optional: false, min: 3, max: 20 })
            validate.validateString({ string: benefit.description, fieldName: 'Description', optional: false, min: 10, max: 50 })

            await db.CompanyBenefit.updateOne({ _id: benefit._id }, benefit)

            res.send(new Success())
        } catch (error) {
            res.send(error)
        }
    })
}