module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.get('/getCompanyBenefits', validateToken([rolesEnum.EMPLOYEE, rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            const result = await db.CompanyBenefit.find()
            res.send(new Success(result))
        } catch (error) {
            res.send(error)
        }
    })
}