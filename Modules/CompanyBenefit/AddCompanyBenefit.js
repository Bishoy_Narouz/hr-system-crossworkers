module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.post('/addCompanyBenefit', validateToken([rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            let benefit = new db.CompanyBenefit({
                title: req.body.title,
                description: req.body.description,
                createdAt: new Date()
            })

            validate.validateString({ string: benefit.title, fieldName: 'Title', optional: false, min: 3, max: 20 })
            validate.validateString({ string: benefit.description, fieldName: 'Description', optional: false, min: 10, max: 50 })

            const result = await benefit.save()

            res.send(new Success(result))
        } catch (error) {
            res.send(error)
        }
    })
}