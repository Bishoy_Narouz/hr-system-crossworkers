module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.post('/sendMessage', validateToken([rolesEnum.HR_ASSISTANT, rolesEnum.MANAGER]), async (req, res) => {
        try {
            let message = new db.Chat({
                text: req.body.text,
                from: req.decoded.userId,
                to: req.body.to,
                createdAt: new Date()
            })

            validate.validateString({ string: req.body.text, fieldName: 'Text', optional: false })
            validate.validateObjectId({ objectId: req.decoded.userId, fieldName: 'Sender Id', optional: false })
            validate.validateObjectId({ objectId: req.body.to, fieldName: 'Send To Id', optional: false })

            const result = await message.save()

            //send broadcast message to all users
            req.app.io.sockets.emit('new-message',
                {
                    _id: result._id,
                    text: result.text,
                    createdAt: result.createdAt
                }
            )

            res.send(new Success(result))
        } catch (error) {
            console.log(error)
            res.send(error)
        }
    })
}