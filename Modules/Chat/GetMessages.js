module.exports = function (router, db, errorMessageEnum, rolesEnum, validate, validateToken, Success, errors) {
    return router.get('/getMessages', validateToken([rolesEnum.EMPLOYEE, rolesEnum.HR_ASSISTANT]), async (req, res) => {
        try {
            const result = await db.Chat.find().sort({ createdAt: 1 })
            res.send(new Success(result))
        } catch (error) {
            res.send(error)
        }
    })
}