import rolesEnum from '../../Enums/RolesEnum'
import errors from '../../MiddleWare/Errors/index'
import errorMessageEnum from '../../Enums/ErrorMessageEnum'

module.exports = (mongoose) => {
    const userSchema = new mongoose.Schema({
        username: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            min: 6,
            max: 12
        },
        email: {
            type: String,
            required: true,
            unique: true,
            trim: true
        },
        fullName: {
            type: String,
            required: true,
            trim: true,
            max: 30
        },
        mobile: {
            type: String,
            required: true,
            trim: true,
            min: 10,
            max: 12
        },
        password: {
            type: String,
            required: true,
            trim: true,
            min: 8,
            max: 25
        },
        role: {
            type: String,
            required: true,
            trim: true,
            enum: [rolesEnum.MANAGER, rolesEnum.HR_ASSISTANT]
        },
        manager: {
            type: mongoose.Schema.Types.ObjectId, ref: 'Manager',
            required: false
        },
        blocked: {
            type: Boolean,
            default: false
        },
        createdAt: {
            type: Date,
            required: true
        }
    })

    userSchema.index({ _id: 1, username: 1, email: 1 })

    userSchema.pre('save', async function (next) {
        var self = this
        const isUserExist = await User.findOne({ $and: [{ username: self.username }, { email: self.email }] })
        if (isUserExist) {
            next(new errors.DuplicateError({ message: errorMessageEnum.USER_IS_DUPLICATED }))
        } else {
            next()
        }
    })

    userSchema.pre('updateOne', async function (next) {
        var self = this
        const isUserExist = await User.findOne({ _id: self._conditions._id })
        if (!isUserExist) {
            next(new errors.NotFound({ message: errorMessageEnum.USER_IS_NOT_FOUND }))
        } else {
            const isUserDuplicated = await User.findOne(
                {
                    $and: [
                        { _id: { $ne: self._update._id } },
                        {
                            $or: [
                                { email: self._update.email },
                                { username: self._update.username }
                            ]
                        }
                    ]
                }
            )
            if (isUserDuplicated) {
                next(new errors.DuplicateError({ message: errorMessageEnum.USER_IS_DUPLICATED }))
            } else {
                next()
            }
        }
    })

    const User = mongoose.model('User', userSchema)

    return User
}