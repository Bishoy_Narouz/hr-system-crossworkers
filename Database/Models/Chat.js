module.exports = (mongoose) => {
    const chatSchema = new mongoose.Schema({
        text: {
            type: String,
            required: true,
            trim: true
        },
        from: {
            type: mongoose.Schema.Types.ObjectId, ref: 'User',
            required: true
        },
        to: {
            type: mongoose.Schema.Types.ObjectId, ref: 'User',
            required: true
        },
        createdAt: {
            type: Date,
            require: true
        }
    })

    chatSchema.index({ _id: 1, to: 1, from: 1 })

    const Chat = mongoose.model('Chat', chatSchema)

    return Chat
}