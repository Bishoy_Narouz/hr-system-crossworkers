import errors from '../../MiddleWare/Errors/index'
import errorMessageEnum from '../../Enums/ErrorMessageEnum'

module.exports = (mongoose) => {
    const managerSchema = new mongoose.Schema({
        name: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            min: 6,
            max: 12
        },
        createdAt: {
            type: Date,
            required: true
        }
    })

    managerSchema.index({ _id: 1, name: 1 })

    managerSchema.pre('save', async function (next) {
        var self = this
        const isManagerExist = await Manager.findOne({ name: self.name })
        if (isManagerExist) {
            next(new errors.DuplicateError({ message: errorMessageEnum.MANAGER_IS_DUPLICATED }))
        } else {
            next()
        }
    })

    managerSchema.pre('updateOne', async function (next) {
        var self = this
        const isManagerExist = await Manager.findOne({ _id: self._conditions._id })
        if (!isManagerExist) {
            next(new errors.NotFound({ message: errorMessageEnum.MANAGER_IS_NOT_FOUND }))
        } else {
            const isManagerDuplicated = await Manager.findOne(
                {
                    $and: [
                        { _id: { $ne: self._update._id } },
                        { name: self._update.name }
                    ]
                }
            )
            if (isManagerDuplicated) {
                next(new errors.DuplicateError({ message: errorMessageEnum.MANAGER_IS_DUPLICATED }))
            } else {
                next()
            }
        }
    })

    const Manager = mongoose.model('Manager', managerSchema)

    return Manager
}