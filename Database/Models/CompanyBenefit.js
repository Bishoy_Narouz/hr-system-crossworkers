import rolesEnum from '../../Enums/RolesEnum'
import errors from '../../MiddleWare/Errors/index'
import errorMessageEnum from '../../Enums/ErrorMessageEnum'

module.exports = (mongoose) => {
    const companyBenefitSchema = new mongoose.Schema({
        title: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            min: 6,
            max: 12
        },
        description: {
            type: String,
            required: true,
            trim: true,
            min: 10,
            max: 50
        },
        createdAt: {
            type: Date,
            required: true
        }
    })

    companyBenefitSchema.index({ _id: 1, title: 1 })

    companyBenefitSchema.pre('save', async function (next) {
        var self = this
        const isCompanyBenefitExist = await CompanyBenefit.findOne({ title: self.title })
        if (isCompanyBenefitExist) {
            next(new errors.DuplicateError({ message: errorMessageEnum.COMPANY_BENEFIT_IS_DUPLICATED }))
        } else {
            next()
        }
    })

    companyBenefitSchema.pre('updateOne', async function (next) {
        var self = this
        const isCompanyBenefitExist = await CompanyBenefit.findOne({ _id: self._conditions._id })
        if (!isCompanyBenefitExist) {
            next(new errors.NotFound({ message: errorMessageEnum.COMPANY_BENEFIT_IS_NOT_FOUND }))
        } else {
            const isCompanyBenefitDuplicated = await CompanyBenefit.findOne(
                {
                    $and: [
                        { _id: { $ne: self._update._id } },
                        { title: self._update.title }
                    ]
                }
            )
            if (isCompanyBenefitDuplicated) {
                next(new errors.DuplicateError({ message: errorMessageEnum.COMPANY_BENEFIT_IS_DUPLICATED }))
            } else {
                next()
            }
        }
    })

    const CompanyBenefit = mongoose.model('CompanyBenefit', companyBenefitSchema)

    return CompanyBenefit
}