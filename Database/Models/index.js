import mongoose from 'mongoose'
import path from 'path'
import fs from 'fs'

const modelsPath = __dirname
let db = {}
fs
    .readdirSync(modelsPath)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        if (file !== 'index.js') {
            let model = require(`./${file}`)(mongoose)
            db[model.modelName] = model
        }
    })

module.exports = db

